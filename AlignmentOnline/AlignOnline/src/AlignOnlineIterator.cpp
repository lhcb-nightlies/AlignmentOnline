/*
 * AligOnlIterator.cpp
 *
 *  Created on: Oct 6, 2014
 *      Author: beat
 */
#include <fstream>
#include <thread>
#include <chrono>

#include <boost/filesystem.hpp>

#include <GaudiKernel/Service.h>
#include <GaudiKernel/IPublishSvc.h>
#include <GaudiKernel/IDetDataSvc.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/ToolHandle.h>

#include <GaudiAlg/GaudiTool.h>

#include <DetDesc/RunChangeIncident.h>

#include "OnlineAlign/IAlignSys.h"
#include "OnlineAlign/IAlignUser.h"
#include "RTL/rtl.h"

#include "AlignmentInterfaces/IAlignUpdateTool.h"
#include "AlignmentInterfaces/IWriteAlignmentConditionsTool.h"
#include "AlignmentMonitoring/CompareConstants.h"

#include "AlignOnlineXMLCopier.h"
#include "ASDCollector.h"


//#include <stdio.h>
//#include <stdlib.h>

namespace {
   using std::string;
   using std::vector;
   using std::map;
   using std::to_string;
   using std::ofstream;
   using std::make_pair;
   using std::endl;
}

class AlignOnlineIterator: public GaudiTool, virtual public LHCb::IAlignIterator
{
  public:
    AlignOnlineIterator(const string & type, const string & name,
        const IInterface * parent);
    //      StatusCode init(string, string);
    StatusCode i_start() override;
    StatusCode i_run();
    StatusCode initialize() override;
    StatusCode finalize() override;
    StatusCode stop() override;
    //      StatusCode de_init();
    StatusCode queryInterface(const InterfaceID& riid, void** ppvIF) override;
  private:
    StatusCode writeWrappers() const ;
    StatusCode writeWrapper(const std::string& det) const ;
    LHCb::IAlignDrv *m_parent;
    lib_rtl_thread_t m_thread;
    string m_PartitionName;
    string m_runType;
    ToolHandle<Al::IAlignUpdateTool> m_alignupdatetool;
    ToolHandle<IWriteAlignmentConditionsTool> m_xmlwriter;
    unsigned int m_maxIteration;
    ASDCollector m_asdCollector;
    unsigned int m_iteration;
    string m_onlinexmldir;
    string m_alignworkdir; // directory where all jobs run
    string m_runningdir ;  // sub directory where this job runs
    string m_runningxmldir ;  // directory where the iterator stores the xml
    bool m_keepIterXml ;
    int m_refRunNr;
    vector<AlignOnlineXMLCopier*> m_xmlcopiers;
    SmartIF<IPublishSvc> m_PubSvc;
    map<string, string*> m_condmap;
    string m_ServInfix;
    vector<string> m_subDets;
    SmartIF<IUpdateManagerSvc> m_updateMgrSvc ; ///< Pointer to update manager svc
    unsigned int m_wrnLo;
    unsigned int m_wrnHi;
    string m_align_message; // < string with error messages or info to publish with DIM service
    unsigned int m_minEvents;
};

DECLARE_TOOL_FACTORY(AlignOnlineIterator)

static AlignOnlineIterator *IteratorInstance;

extern "C"
{
  int AlignOnlineIteratorThreadFunction(void *t)
  {
    AlignOnlineIterator *f = (AlignOnlineIterator*) t;
    IteratorInstance = f;
    f->i_run();
    return 1;
  }

}

AlignOnlineIterator::AlignOnlineIterator(const string & type,
    const string & name, const IInterface * parent) :
    GaudiTool(type, name, parent), m_alignupdatetool("Al::AlignUpdateTool"), m_xmlwriter(
        "WriteMultiAlignmentConditionsTool")
{
  declareInterface<LHCb::IAlignIterator>(this);
  declareProperty("PartitionName", m_PartitionName = "LHCbA");
  declareProperty("MaxIteration", m_maxIteration = 10);
  declareProperty("ASDDir", m_asdCollector.m_dir);
  declareProperty("ASDFilePattern", m_asdCollector.m_filePatt);
  declareProperty("OnlineXmlDir", m_onlinexmldir = "/group/online/alignment");
  declareProperty("AlignXmlDir", m_alignworkdir = "/group/online/AligWork");
  declareProperty("ServiceInfix", m_ServInfix = "TrackerAlignment/");
  declareProperty("SubDetectors", m_subDets);
  declareProperty("KeepIterXml", m_keepIterXml = true ) ;
  declareProperty("RunType", m_runType = "NotDefined");
  declareProperty("ReferenceRunNr",m_refRunNr);
  declareProperty("MinimumEvents", m_minEvents = 40000);

  m_iteration = 0;
  IInterface *p = (IInterface*) parent;
  StatusCode sc = p->queryInterface(LHCb::IAlignDrv::interfaceID(),
      (void**) (&m_parent));
}

StatusCode AlignOnlineIterator::initialize()
{
  debug() << "AlignOnlineIterator::initialize()" << endmsg;
  debug() << "ASDDir         : " << m_asdCollector.m_dir << endmsg;
  debug() << "ASDFilePattern : " << m_asdCollector.m_filePatt << endmsg;
  debug() << "PartitionName  : " << m_PartitionName << endmsg;
  debug() << "MaxIteration    : " << m_maxIteration << endmsg;
  debug() << "ReferenceRunNumber  :" << m_refRunNr <<endmsg;

  StatusCode sc = GaudiTool::initialize();
  if (sc.isSuccess())
  {
    sc = m_alignupdatetool.retrieve();
    if (!sc.isSuccess())
      error() << "Cannot retrieve alignupdatetool" << endmsg;
  }
  if (m_refRunNr == -1)
  {
    error() << "reference Run Number not defined. seems that there are no runs specified in the RunInfo" <<endmsg;
  }
  m_PubSvc = service("LHCb::PublishSvc", true);
  if (!m_PubSvc)
  {
     return Error("cannot retrieve the Publishing Service", StatusCode::FAILURE);
  }

  m_updateMgrSvc = service("UpdateManagerSvc",false);
  if (!m_updateMgrSvc)
     return Error("==> Failed to retrieve UpdateManagerSvc", StatusCode::FAILURE);

  if (sc.isSuccess())
  {
    sc = m_xmlwriter.retrieve();
    if (!sc.isSuccess())
      error() << "Cannot retrieve xmlwriter" << endmsg;
    if (m_xmlwriter->FSMState() < Gaudi::StateMachine::INITIALIZED) m_xmlwriter->initialize();
    IProperty* prop = 0;
    sc = m_xmlwriter->queryInterface(IProperty::interfaceID(), (void**)&prop);
    if (sc.isSuccess()) {
       prop->setProperty("OnlineMode", "True");
    }
  }

  // instantiate the objects that will take care of copying and versioning files
  m_runningdir = m_alignworkdir + "/running" ;
  if (boost::filesystem::exists(m_runningdir))
    boost::filesystem::remove_all(m_runningdir);
  if (!boost::filesystem::exists(m_runningdir))
    boost::filesystem::create_directory(m_runningdir);
  m_runningxmldir = m_runningdir + "/xml" ;
  if (!boost::filesystem::exists(m_runningxmldir))
    boost::filesystem::create_directory(m_runningxmldir);

  vector<string> condnames;
  for (const auto& sd: m_subDets) {
     condnames.push_back(sd + "/" + sd + "Global");
     condnames.push_back(sd + "/" + sd + "Modules");
  }
  for (const auto& i : condnames) {
    string *s = new string("");
    m_condmap.insert(make_pair(i, s));
    AlignOnlineXMLCopier* acpy = new AlignOnlineXMLCopier(m_onlinexmldir, m_runningxmldir, i, &info());
    m_PubSvc->declarePubItem(m_ServInfix + i, *s);
    m_xmlcopiers.push_back(acpy);
  }
  fflush(stdout);

  // Define error DIM service
  m_align_message = "INFO: " + m_runType + " Initializing align messanger"; // This message will be overwritten
  m_PubSvc->declarePubItem("AlignmentMessenger", m_align_message);

  // set reference base
  m_parent->setReferenceBase(0);

  return sc;
}

StatusCode AlignOnlineIterator::finalize()
{
  m_PubSvc->undeclarePubAll();
  for (auto& j : m_condmap)
  {
    delete j.second;
  }
  m_condmap.clear();
  for (auto& i : m_xmlcopiers)
  {
    delete (i);
  }
  m_xmlcopiers.clear();
  m_alignupdatetool.release().ignore();
  m_xmlwriter.release().ignore();
  return GaudiTool::finalize();
}

StatusCode AlignOnlineIterator::i_run()
{
  StatusCode sc = StatusCode::SUCCESS;
  // only called once

  int runnr = m_refRunNr;
  IDetDataSvc* detDataSvc(0);
  sc = service("DetectorDataSvc", detDataSvc, false);
  if (sc.isFailure())
  {
    error() << "Could not retrieve DetectorDataSvc" << endmsg;
    return sc;
  }
  IIncidentSvc* incSvc = svc<IIncidentSvc>("IncidentSvc");

  Al::IAlignUpdateTool::ConvergenceStatus convergencestatus = Al::IAlignUpdateTool::Unknown;

  // Constants
  std::map<std::string, double> initAlConsts, finalAlConsts;

  while (m_iteration <= m_maxIteration)
  {
    debug() << "wait for analyzers" << endmsg;
    m_parent->waitRunOnce();

    debug() << "Iteration " << m_iteration << endmsg;
    // 4. read ASDs and compute new constants
    debug() << "Collecting ASD files" << endmsg;
    Al::Equations equations(0);
    auto nASD = m_asdCollector.collectASDs(equations);

    if (!nASD) {
       info() << "Collected no ASDs, sleeping 5 seconds and trying again." << endmsg;
       std::this_thread::sleep_for(std::chrono::seconds(5));
       nASD = m_asdCollector.collectASDs(equations);
    }

    info() << "Collected " << nASD << " ASDs: " << endmsg
           << "numevents = " << equations.numEvents() << endmsg
           << "numtracks = " << equations.numTracks() << endmsg
           << "numvertex = " << equations.numVertices() << endmsg
           << "numhits   = " << equations.numHits() << endmsg;

    if (equations.numEvents() < m_minEvents) {
       error() << "Collected " << nASD << " ASDs: numevents = " << equations.numEvents()
               << " is too small, need at least " << m_minEvents << "." << endmsg;
       sc = StatusCode::FAILURE;
       break;
    }
//    runnr = equations.firstRun();

    // FIXME: fire incident in the run changehandler to read the xml:
    // otherwise it will just use the geometry from the snapshot
    // Don't know if we need all of this ... Need to think.
    // Update 2015/06/11: we definitely need the 'newEvent'.
    detDataSvc->setEventTime(equations.lastTime() + m_iteration);
    warning() << "equations lastTime(): " << equations.lastTime() << endmsg
              << "initTime()" << equations.initTime() << endmsg;
    incSvc->fireIncident(RunChangeIncident(name(), runnr));
    m_updateMgrSvc->newEvent(detDataSvc->eventTime()) ;

    // Now call the update tool
    debug() << "Calling AlignUpdateTool" << endmsg;
    sc = m_alignupdatetool->process(equations, convergencestatus);
    if (!sc.isSuccess())
    {
      error() << "Error calling alignupdate tool" << endmsg;
      break;
    }

    // Store alignment constants
    if (m_iteration == 1) // first iteration store the initial
      initAlConsts = m_alignupdatetool->getAlignmentConstants(false);

    // copy the xml directory to a directory with the iteration name
    if( m_keepIterXml ) {
      const std::string xmliterdir = m_runningdir + "/Iter" + to_string(m_iteration);
      boost::system::error_code ec;
      boost::filesystem::rename(m_runningxmldir, xmliterdir,ec);
      if (ec.value() != 0)
      {
        error() << "Rename from " << m_runningxmldir << " to " << xmliterdir << "failed: Return"
            << ec.value()<<" " << ec.message()<<". Retrying..."<< endmsg;
        boost::filesystem::remove_all(xmliterdir,ec);
        boost::filesystem::rename(m_runningxmldir, xmliterdir,ec);
        if (ec.value() != 0)
          error() << "Rename from " << m_runningxmldir << " to " << xmliterdir << "failed again: Return"
              << ec.value()<<" " <<ec.message() << "giving up..."<<endmsg;
      }
    }

    // write the xml
    debug() << "writing xml files" << endmsg;
    sc = m_xmlwriter->write("run" + to_string(runnr) ) ;
    if (!sc.isSuccess())
    {
      error() << "Error writing xml files" << endmsg;
      break;
    }

    // Write the wrappers for the generated conditions
    if (sc.isSuccess()) sc = writeWrappers() ;

    // writes a little file with number of iteration step
    debug() << "calling parent->writeReference()" << endmsg;
    m_parent->writeReference();

    // break the loop if converged or maximum number of iterations reached
    if (convergencestatus == Al::IAlignUpdateTool::Converged || m_iteration > m_maxIteration)
    {
      break;
    }
    ++m_iteration;
    // start the analyzers and wait
    m_asdCollector.setTime();
    m_parent->doContinue();
  }
  string *s;
  if (sc.isSuccess() && m_iteration > 1 && convergencestatus == Al::IAlignUpdateTool::Converged)
  {
    // if converged, check whether alignment needs to be updated by comparing the constants
    //
    // get the latest constants
    finalAlConsts = m_alignupdatetool->getAlignmentConstants(true);
    // compare them to initial
    Alignment::AlignmentMonitoring::CompareConstants cmp(initAlConsts,finalAlConsts);
    cmp.Compare();
    MsgStream err(msgSvc(), name());
    if ( msgLevel(MSG::DEBUG) ) cmp.PrintWarnings(1,err);
    // print the elements that showed significant variation
    cmp.PrintWarnings(2, err);
    cmp.PrintWarnings(3, err);
    if (cmp.GetNumWarnings(3)){// changes to big, make it fail
      error() << "Alignment converged but constants changes is unreasonably large" << endmsg;
      if (m_runType == string("Velo") || m_runType == string("Tracker")){
        m_align_message = "ERROR: " + m_runType + " Alignment converged but constants changes is unreasonably large; please call the Alignment piquet | " + cmp.GetWarningMessages(3);}
        if (m_runType == string("Muon")){
          m_align_message = "WARNING: " + m_runType + " Alignment converged but constants changes is unreasonably large |" + cmp.GetWarningMessages(3);}
      sc = StatusCode::FAILURE;
    }
    else if ( !( (m_runType == string("Velo") && cmp.GetNumWarnings(2, "Velo(Left|Right)\\..*")) ||
		 (m_runType == string("Tracker") && cmp.GetNumWarnings(2)>3) ) ){ // no significant change
      warning() << "Alignment converged in more than one iteration but constants didn't change significantly."
		<< endmsg;
    m_align_message = "INFO: " + m_runType + " Alignment converged in more than one iteration but constants didn't change significantly";
    } else {
      // after last update, if more than one iteration and successfully converged
      for (auto& i : m_xmlcopiers)
	{
	  StatusCode thissc = i->copyToOnlineArea();
	  info() << "Condition: " << i->condition() << " Filename: "
		 << i->copybackfilename() << endmsg;
	  auto j = m_condmap.find(i->condition());
	  s = (j->second);
	  //*s = to_string(runnr) + " " + i->copybackfilename();
	  *s = to_string(runnr) + " v" + to_string(i->version());

	  if (!thissc.isSuccess())
	    {
	      error() << "Error copying file to online area" << endmsg;
	    }

	}
      warning() << "Alignment updated." << endmsg;
      m_align_message = "INFO: " + m_runType + " Alignment updated";
    } // end of check on the constants
  } // end check alignmet converged in more than 1 iterations
  else
  {
    if (sc.isSuccess() && m_iteration <= 1)
    {
      warning() << "Alignment didn't change." << endmsg;
      m_align_message = "INFO: " + m_runType + " Alignment didn't change";
    }
    else
    {
      warning() << "Alignment procedure failed." << endmsg;
      m_align_message = "ERROR: " + m_runType + " Alignment procedure failed; please call the Alignment piquet";
    }
  } //end check alignment converged or failed in <=1 iteration
  m_PubSvc->updateAll();
  fflush(stdout);

  // move the 'running' dir to a dirname with current run
  string rundir = m_alignworkdir + "/"+ m_runType+"/run" + to_string(runnr);
  if (! (boost::filesystem::exists(m_alignworkdir + "/"+ m_runType)) )
    boost::filesystem::create_directory(m_alignworkdir + "/"+ m_runType);
  if (boost::filesystem::exists(rundir)) {
    int i(1);
    while(boost::filesystem::exists(rundir+"_v"+std::to_string(i))) ++i;
    rundir = rundir+"_v"+std::to_string(i);
  }
  //    boost::filesystem::remove_all(rundir);
  boost::filesystem::rename(m_runningdir,rundir);

  //fflush(stdout);
  m_parent->doStop();
  return sc;
}

StatusCode AlignOnlineIterator::i_start()
{
  StatusCode sc = StatusCode::SUCCESS;
  // called only once
  // don't touch this
  debug() << "Iteration " << ++m_iteration << endmsg;

  // 1. writes a little file with number of iteration step
  debug() << "calling parent->writeReference()" << endmsg;
  m_parent->writeReference();

  // 2. copy the files from the online area. if this doesn't work,
  // dump the current database.
  for (auto& i : m_xmlcopiers)
  {
    StatusCode thissc = i->copyFromOnlineArea();
    if (!thissc.isSuccess())
    {
      warning() << "Cannot find input xml file \'" << i->onlinefilename() << "\'" << endmsg;
      sc = StatusCode::FAILURE;
    }
  }
  fflush(stdout);

  // if some of the input files are missing, bootstrap this by writing
  // from the database.
  if (!sc.isSuccess())
  {
    debug() << "writing xml files in i_start to bootstrap alignment" << endmsg;
    sc = m_xmlwriter->write();
    if (!sc.isSuccess())
      error() << "Error writing xml files" << endmsg;
  }

  // Write the wrappers for the generated conditions, this only needs to happen once.
  if (sc.isSuccess()) sc = writeWrappers() ;

  // 3. start the analyzers and wait
  debug() << "wait for analyzers" << endmsg;
  m_asdCollector.setTime();
  ::lib_rtl_start_thread(AlignOnlineIteratorThreadFunction, this, &m_thread);
  return sc;
}

StatusCode AlignOnlineIterator::stop()
{
  // called only once
  ::lib_rtl_delete_thread(m_thread);
  return StatusCode::SUCCESS;
}

StatusCode AlignOnlineIterator::queryInterface(const InterfaceID& riid,
    void** ppvIF)
{
  if (LHCb::IAlignIterator::interfaceID().versionMatch(riid))
  {
    *ppvIF = (IAlignIterator*) this;
    addRef();
    return StatusCode::SUCCESS;
  }
  return GaudiTool::queryInterface(riid, ppvIF);
}

StatusCode AlignOnlineIterator::writeWrappers() const
{
  StatusCode sc = StatusCode::SUCCESS ;
  for (const auto& sd: m_subDets) {
    sc = writeWrapper(sd);
    if (!sc.isSuccess()) {
      error() << "Error writing wrapper file for " << sd << endmsg;
      break;
    }
  }
  return sc ;
}

StatusCode AlignOnlineIterator::writeWrapper(const string& sd) const
{
   string filename = m_runningxmldir + "/" + sd + ".xml";
   ofstream xml(filename);
   if (xml.fail() ){
      return Warning(string("Failed to open wrapper file ") + filename, StatusCode::FAILURE);
   }
   // XML header and links
   xml << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>" << endl
       << "<!DOCTYPE DDDB SYSTEM \"conddb:/DTD/structure.dtd\"" << endl
       << "[<!ENTITY " << sd << "GlobalLabel SYSTEM \"" << sd << "/" << sd << "Global.xml\">" << endl
       << " <!ENTITY " << sd << "ModLabel SYSTEM \"" << sd << "/" << sd << "Modules.xml\">]" << endl
       << ">" << endl
   // DDDB open
       << "<DDDB>" << endl
   // labels
       << "&" << sd << "GlobalLabel;" << endl
       << "&" << sd << "ModLabel;" << endl
   // DDDB close
       << "</DDDB>" << endl;
   return StatusCode::SUCCESS;
}
