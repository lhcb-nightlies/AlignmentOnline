#!/usr/bin/env python
import subprocess
import sys, time, os

this_file_dir = os.path.dirname(os.path.realpath(__file__))

def findStartTime_old(run):
    '''
    Return initial time run reading from the runDB
    '''
    try:
        import rundb
    except ImportError:
        sys.path.append('/group/online/rundb/RunDatabase/python')
        import rundb
    
    db = rundb.RunDB()
    
    startTime = db.getrun(run)[0]['startTime']
        
    return (int(time.mktime(time.strptime(startTime,'%Y-%m-%d %H:%M:%S.0000')))+2)*10**9


def findStartTime(run):
    '''
    Return initial time run reading from the handshake file
    '''
    start_time = open('/group/online/hlt/conditions/LHCb/NoYear/{0}/{1}/CondDBXfer_done'.format(int(run)/1000, run)).read().split()[0]
        
    return (int(start_time)+2)*10**9


def parseConditions(inFile_name):
    import re
    str_conditions = open(inFile_name).read()
    conditions = re.findall(r'BEGIN CONDITION\n([^`]*?)\n([^`]*?)\nEND CONDITION', str_conditions)
    dict_conditions = {}
    for cond, value in conditions:
        dict_conditions[cond] = value
    return dict_conditions


def diffConditions(fileNames):
    
    dict_online = parseConditions(fileNames['online'])
    dict_offline = parseConditions(fileNames['offline'])
    dict_oracle = parseConditions(fileNames['oracle'])
    
    shared_items_offline_online = set(dict_offline.items()) & set(dict_online.items())
    shared_items_offline_oracle = set(dict_offline.items()) & set(dict_oracle.items())
    
    diff_offline_wrt_online = dict(set(dict_offline.items()) - shared_items_offline_online)
    diff_online = dict(set(dict_online.items()) - shared_items_offline_online)
    diff_offline_wrt_oracle = dict(set(dict_offline.items()) - shared_items_offline_oracle)
    diff_oracle = dict(set(dict_oracle.items()) - shared_items_offline_oracle)

    str_diff = ''''''
    if diff_offline_wrt_online:
        str_diff += '\n'
        str_diff += '##################################\n'
        str_diff += '###   DIFF ONLINE - OFFLINE    ###\n'
        str_diff += '##################################\n'
        for cond in diff_offline_wrt_online:
            str_diff += '\n'+'#'*50
            str_diff += '\n\nCondition:\n'
            str_diff += cond
            str_diff += '\n\nOnline:\n'
            str_diff += diff_online[cond]
            str_diff += '\n\nOffline:\n'
            str_diff += diff_offline_wrt_online[cond]
    if diff_offline_wrt_oracle:
        str_diff += '\n\n'
        str_diff += '##################################\n'
        str_diff += '###   DIFF ORACLE - GIT DB     ###\n'
        str_diff += '##################################\n'
        for cond in diff_offline_wrt_oracle:
            str_diff += '\n'+'#'*50
            str_diff += '\n\nCondition:\n'
            str_diff += cond
            str_diff += '\n\nOracle:\n'
            str_diff += diff_oracle[cond]
            str_diff += '\n\nGit database:\n'
            str_diff += diff_offline_wrt_oracle[cond]                                          

    return str_diff


def diffOnlineOffline(run, delCond = True):
    '''
    Function that check that the same calibration constants are applied online and offline
    If no difference return false else return diff of files produced by Brunel
    '''

    import time, shlex
    
    fileNames = dict( offline = '/group/online/AligWork/CheckConstants/conditions_offline.txt',
                      online = '/group/online/AligWork/CheckConstants/conditions_online.txt',
                      oracle = '/group/online/AligWork/CheckConstants/conditions_oracle.txt')

    for fileName in fileNames.values():
        if os.path.exists(fileName):
            os.remove(fileName)

    startTime = findStartTime(run)
    print 'startTime:', startTime

    #year = time.gmtime(startTime/10**9)[0]
    #run_cond_dir = '/group/online/hlt/conditions/LHCb/{0}/{1}'.format(year, run)

    run_cond_dir = '/group/online/hlt/conditions/LHCb/NoYear/{0}/{1}'.format(str(int(int(run)/1000)), run)
    try:
        execfile(os.path.join(run_cond_dir, 'HLT2Params.py'))
        locals()['DDDBTag'], locals()['CondDBTag']
    except (IOError, KeyError):
        print 'WARNING: Skipping due to missing HLT2Params'
        raise AssertionError('WARNING: Skipping due to missing HLT2Params')


    env = os.environ.copy()
    env['PYTHONPATH'] = (env['PYTHONPATH']
                         + ':' + run_cond_dir
                         + ':/group/online/hlt/conditions/RunChangeHandler')
    command = 'python ' + os.path.join(this_file_dir, 'printConds.py') + ' {online_script} {startTime} {runNumber} {outFile}'
        
    opts_online = dict(
        startTime = startTime,
        runNumber = run,
        online_script = '--online',
        outFile = "'{0}'".format(fileNames['online']))
    
    opts_offline = dict(
        startTime = startTime,
        runNumber = run,
        online_script = '',
        outFile = "'{0}'".format(fileNames['offline']))

    opts_oracle = dict(
        startTime = startTime,
        runNumber = run,
        online_script = '--oracle',
        outFile = "'{0}'".format(fileNames['oracle']))

    print '######################################'
    print '###   RUN WITH ONLINE CONDITIONS   ###'
    print '######################################'
    sys.stdout.flush()
    cmd = shlex.split(command.format(**opts_online))
    subprocess.call(cmd, env = env)
    assert(os.path.exists(fileNames['online']))
    print '#######################################'
    print '###   RUN WITH OFFLINE CONDITIONS   ###'
    print '#######################################'
    sys.stdout.flush()
    cmd = shlex.split(command.format(**opts_offline))
    subprocess.call(cmd, env = env)
    assert(os.path.exists(fileNames['offline']))
    print '######################################'
    print '###   RUN WITH ORACLE CONDITIONS    ###'
    print '######################################'
    sys.stdout.flush()
    cmd = shlex.split(command.format(**opts_oracle))
    subprocess.call(cmd, env = env)
    assert(os.path.exists(fileNames['oracle']))
    print '##################################'
    print '###   DIFF ONLINE - OFFLINE    ###'
    print '##################################'
    difference = diffConditions(fileNames)
    if difference:
        print difference
    else:
        print 'No difference'
   
    if delCond:
        for fileName in fileNames.values():
            if os.path.exists(fileName):
                os.remove(fileName)

    return difference

if __name__ == '__main__':

    import argparse, sys
    parser = argparse.ArgumentParser(description ="Macro that check that for a particular run the online and offline conditions are the same")
    parser.add_argument('run',help='run to process')
    parser.add_argument('--noDel',help='do not delete conditions file', action = 'store_true')
    args = parser.parse_args()
    
    diff = diffOnlineOffline(args.run, delCond=not args.noDel)
    
    if not diff:
        sys.exit(0)
    else:
        sys.exit(1)
