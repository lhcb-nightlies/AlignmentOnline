"""
     Run Brunel in the online environment

     @author M.Frank
"""
__version__ = "$Id: BrunelOnline.py,v 1.25 2010/11/09 12:20:55 frankb Exp $"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import importlib
import os, sys, re

os.environ['GITCONDDBPATH'] = '/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb.test'

import Configurables as Configs
import Gaudi.Configuration as Gaudi
import GaudiKernel
from GaudiKernel.ProcessJobOptions import PrintOff,InstallRootLoggingHandler,logging


from Configurables import CondDB, GaudiSequencer, EventPersistencySvc, \
    HistogramPersistencySvc, EventLoopMgr, OutputStream, Gaudi__SerializeCnvSvc, \
    DstConf

#PrintOff(999)
InstallRootLoggingHandler(level=logging.CRITICAL)

processingType ='DataTaking'

GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
requirement = None

debug = 0
def dummy(*args,**kwd): pass

MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

configureBrunelOutput = None
from TAlignment.AlignmentScenarios import *
from TAlignment.TrackSelections import *
from TAlignment.AlignmentScenarios import *

def staticVar(varname, value):
    def decorate(func):
        setattr(func, varname, value)
        return func
    return decorate

@staticVar("Online", None)
def importOnline():
  if importOnline.Online:
    return importOnline.Online
  print "RUNINFO in environment: %s" % os.environ.get("RUNINFO", "False")
  if 'RUNINFO' in os.environ:
    runinfo = os.path.dirname(os.environ['RUNINFO'])
    sys.path.insert(1, runinfo)
    Online = importlib.import_module('OnlineEnv')
    sys.path.remove(runinfo)
  else:
    import OnlineEnv as Online
  importOnline.Online = Online
  return Online

def EscherCommon(true_online_version, alignment_module):
  import GaudiConf.DstConf
  import Escher.Configuration
  from Configurables import MagneticFieldSvc
  from Configurables import TAlignment
  from TAlignment.VertexSelections import configuredPVSelection
  from Configurables import RunChangeHandlerSvc
  OnlineEnv = importOnline()
  import ConditionsMap

  escher = Escher.Configuration.Escher()
  #escher.DataType = '2016'
  escher.DataType = '2017'
  escher.DDDBtag   = ConditionsMap.DDDBTag
  escher.CondDBtag = ConditionsMap.CondDBTag
  escher.OnlineMode = True
  escher.UseDBSnapshot = True
  escher.DBSnapshotDirectory = "/group/online/hlt/conditions"

  #import OnlineEnvBase
  #escher.DBSnapshotDirectory ='/group/online/hlt/conditions/LHCb/NoYear/'+str(int(OnlineEnvBase.DeferredRuns[0])/1000) +'/'+OnlineEnvBase.DeferredRuns[0]+'/'

  #other possible choices of database tags configuration
  #escher.DDDBtag   = OnlineEnv.DDDBTag
  #escher.CondDBtag = OnlineEnv.CondDBTag
  #escher.CondDBtag = "cond-20161011"
  #escher.CondDBtag = "cond-20170510"
  #To run on 2016 data this tag needed
  #escher.CondDBtag = "cond-20170325"

  if hasattr(OnlineEnv, "AlignXmlDir"):
    escher.OnlineAligWorkDir = os.path.join(OnlineEnv.AlignXmlDir, 'running')

  sys.path.insert(2, os.path.dirname(OnlineEnv.ConditionsMapping))

  if escher.DataType in ['2016', '2015']:
      import Online as OnlineConds 
  else :
      import Hlt1CondMap as OnlineConds

  #print '[ERROR]',OnlineConds.ConditionMap
  handlerConditions = OnlineConds.ConditionMap

  ## if true_online_version and os.environ['PARTITION_NAME'] == 'TEST':
  ##   re_year = re.compile('(201\d)')
  ##   for k, v in handlerConditions.items():
  ##     handlerConditions[re_year.sub('2014', k)] = v
  #HACK: replace 2016 with 2015. Must removed to be able to run over new data.
  # handlerConditions = {k.replace('2016', '2015') : v for k, v in OnlineConds.ConditionMap.iteritems()}

  conddb = CondDB()
  conddb.RunChangeHandlerConditions = handlerConditions
  conddb.IgnoreHeartBeat = True
  conddb.EnableRunStampCheck = False
  #conddb.EnableRunChangeHandler = True
  conddb.Tags['ONLINE'] = OnlineEnv.OnlDBTag #needed when using gitDB

  #import RunOption
  #from Configurables import EventClockSvc
  #ecs = EventClockSvc()
  #ecs.addTool(FakeEventTime,"EventTimeDecoder")
  #ecs.EventTimeDecoder.StartRun = RunOption.RunNumber
  #
  #if not escher.UseDBSnapshot and conddb.EnableRunChangeHandler:
  from Configurables import RunChangeHandlerSvc
  rch = RunChangeHandlerSvc()
  rch.ForceUpdate = True
  #    baseloc = escher.DBSnapshotDirectory
  #    rch.Conditions = dict((c, '/'.join([baseloc, f])) for f, cs in conddb.RunChangeHandlerConditions.iteritems() for c in cs)
  #    ApplicationMgr().ExtSvc.append(rch)


  #Only to process 2016 data following line:
  #In case setting the time is needed following examples
  #from Configurables import EventClockSvc
  #EventClockSvc( InitialTime = 1465516800000000000 ) # 10th June 2016
  #EventClockSvc( InitialTime = 1480817100000000000 )#4th December 2016
  #EventClockSvc( InitialTime = 1480557900000000000 )#1st December 2016

#  from Configurables import EventClockSvc
#  runnumbers = OnlineEnv.DeferredRuns
#  #print '[ERROR]', runnumbers
#  from lxml import etree
#  #needed to run on 2016 data:
#  #initialTime = long(1480557900000000000)
#
#  if int(float(runnumbers[0])) < 188360:
#    # parse the online.xml file
#    root = etree.parse('/group/online/hlt/conditions/LHCb/2016/'+runnumbers[0]+'/online.xml')
#    # find all elements of "type" condition
#    conditions = [c for c in root.findall('condition')]
#    # find the condition with name RunParameters
#    param_cond = next((c for c in conditions if c.attrib['name'] == 'RunParameters'), None)
#    # Get the param element (child of the condition element) called
#    # RunStartTime
#    start_time = next((c for c in param_cond.getchildren() if c.attrib['name'] == 'RunStartTime'), None)
#    # Get the start time
#    #print '[ERROR]', long(start_time.text)
#    initialTime = long(int(float(start_time.text)) * 1e9)
#
#  clkSvc = EventClockSvc()
#  clkSvc.InitialTime = initialTime
#
  config_module = importlib.import_module('AlignmentConfigurations.' + alignment_module)
  config_module.configureAlignment()
  return escher

def HostName():
  import socket
  hostname = socket.gethostname()
  return hostname
