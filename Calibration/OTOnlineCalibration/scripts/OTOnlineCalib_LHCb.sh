#!/bin/bash

# environment
#. /group/online/dataflow/cmtuser/AlignmentRelease/setup.x86_64-slc6-gcc49-dbg.vars
export CMTCONFIG=x86_64-centos7-gcc62-dbg
. /group/online/dataflow/cmtuser/AlignmentRelease/setup.${CMTCONFIG}.vars


# Extra stuff for the task
export UTGID;
#export LOGFIFO=/tmp/logOTCalib.fifo;
export LOGFIFO=/tmp/logOTOnlineCalib.fifo;
export PARTITION="LHCb";
export PARTITION_NAME="LHCb";
HOSTUP=`echo $HOSTNAME | tr a-z A-Z`
export TNS_ADMIN=/sw/oracle/10.2.0.4/linux64/network/admin;
export DIM_DNS_NODE=mona09;
export PYTHONPATH=/group/online/dataflow/options/${PARTITION}/RECONSTRUCTION:$PYTHONPATH
export PYTHONPATH=/group/online/hlt/conditions/RunChangeHandler:$PYTHONPATH
export NO_GIT_CONDDB=1;

# Start the task
exec -a ${UTGID} GaudiOnlineExe.exe libGaudiOnline.so OnlineTask -auto \
    -msgsvc=LHCb::FmcMessageSvc \
    -tasktype=LHCb::Class1Task \
    -main=/group/online/dataflow/templates/options/Main.opts \
    -opt=command="\
import os; from Gaudi.Configuration import importOptions;\
importOptions(os.environ['OTONLINECALIBRATIONROOT']+'/scripts/OTOnlineCalib.py');"
